**Integrantes:** Leslie Tatiana Medina 
                 Johan Sebastian Ramirez Celis
                 Joseph Nikolay Arboleda Diaz 

***Collaborative Work Breakdown Structure***

Systems Engineer, Escuela Colombiana de Ingeniería Julio Garavito

Software Architecture, 2017-1, Teacher: Héctor Cadavid Regifo

Description:
The following application performs a Work Breakdown Structure in a collaborative way among multiple users. It allows to use a template with the name of the project and a node (that is the project management), or to start from scratch to realize its own Structure of Breakdown of Work.

It also allows the modification of a specific node, in case something should be changed during the project realization. It also allows you to create relationships between different nodes without affecting the ones you already have.

It is expected that the work carried out may be exported as an image.

Images:
https://bitbucket.org/repo/LoR4zad/images/201964757-readme2.png

https://bitbucket.org/repo/LoR4zad/images/2914194701-readme.png

Link to Heroku: [http://wbs-colaborativo.herokuapp.com/](http://wbs-colaborativo.herokuapp.com/)

*Link to CIRCLECI:* https://circleci.com/bb/j0s3ph/wbs-colaborativo