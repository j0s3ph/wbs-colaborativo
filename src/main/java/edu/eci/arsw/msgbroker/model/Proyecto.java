/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.eci.arsw.msgbroker.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class Proyecto {
    private String nombre;
    private String descripcion;
    private Date fechaCreacion;
    private Date fechaUltimaModificacion;
    private Map<String,WBS> diagramas;

    
    public Proyecto(String name, String description) throws Exception{
        if(name.equals("")) throw new Exception("Por favor colocar un nombre al proyecto");
        if(description.equals("")) throw new Exception("Por favor agregar una descripcion al proyectos");
        nombre=name;
        descripcion=description;
        fechaCreacion=new Timestamp(new Date().getTime());
        fechaUltimaModificacion=new Timestamp(new Date().getTime());
        diagramas= new HashMap<>();
    }
    /**
     * Constructor del proyecto
     */
    public Proyecto(){
    }
    

    public void agregarWBS(WBS d) throws Exception{
        if(diagramas.get(d.getTitulo())==null){
            diagramas.put(d.getTitulo(), d);
            fechaUltimaModificacion=new Timestamp(new Date().getTime());
        }else{
            throw new Exception("El diagrama con titulo "+d.getTitulo()+" ya existe por favor cambie el nombre");
        }
    }
    

    public WBS consultarWBS(String nombre){
            return getWBSs().get(nombre); 
        }


    public String getNombre() {
        return nombre;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
        fechaUltimaModificacion=new Timestamp(new Date().getTime());
    }


    public String getDescripcion() {
        return descripcion;
    }


    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        fechaUltimaModificacion=new Timestamp(new Date().getTime());
    }


    public Date getFechaCreacion() {
        return fechaCreacion;
    }


    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
        fechaUltimaModificacion=new Timestamp(new Date().getTime());
    }


    public Date getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }


    public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }


    public Map<String,WBS> getWBSs() {
        return diagramas;
    }


    public void setWBSs(Map<String,WBS> diagramas) {
        this.diagramas = diagramas;
        fechaUltimaModificacion=new Timestamp(new Date().getTime());
    }
    
    @Override
    public boolean equals(Object o){
        Proyecto o2=(Proyecto) o;
        return nombre.equals(o2.getNombre()) && descripcion.equals(o2.getDescripcion()) && diagramas.equals(o2.getWBSs());
    }
}