/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class WBS {
    protected String titulo;
    protected String descripcion;
    protected Date fechaCreacion;
    protected Date fechaUltimaModificacion;
    private Map<String,Nodo> elementos;
    

    public WBS(String titulo,String descripcion,Date dateCreacion) throws Exception{
        if(titulo.equals("")) throw new Exception("Favor colocar un titulo apropiado al diagrama");
        if(descripcion.equals("")) throw new Exception("Favor colocar una descripcion apropiada al diagrama");
        if(dateCreacion==null) throw new Exception("Favor colocar una fecha de creacion al diagrama");
        this.titulo=titulo;
        this.descripcion=descripcion;
        this.fechaCreacion=dateCreacion;
        this.fechaUltimaModificacion=dateCreacion;
        elementos=new HashMap<>();
   }

   public WBS(String titulo,String descripcion) throws Exception{
        if(titulo.equals("")) throw new Exception("Favor colocar un titulo apropiado al diagrama");
        if(descripcion.equals("")) throw new Exception("Favor colocar una descripcion apropiada al diagrama");
        this.titulo=titulo;
        this.descripcion=descripcion;
        this.fechaCreacion=new Timestamp(new Date().getTime());
        this.fechaUltimaModificacion=new Timestamp(new Date().getTime());
    }
   
    /**
    * Crear diagrama 
    */
    public WBS(){
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        fechaUltimaModificacion=new Timestamp(new Date().getTime());
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String Descripcion) {
        fechaUltimaModificacion=new Timestamp(new Date().getTime());
        this.descripcion = Descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }
    @Override
    public boolean equals(Object o){
        WBS o2=(WBS)o;
        return o2.getTitulo().equals(titulo) && o2.getDescripcion().equals(descripcion);
    }
 

    public void agregarNodo(Nodo e) throws Exception{
        if(elementos.get(e.getName())==null){
            fechaUltimaModificacion=new Timestamp(new Date().getTime());
            getNodos().put(e.getName(), e);
        }else{
             throw new Exception("El elemento con nombre "+e.getName()+" ya existe por favor cambiele el nombre");
        }
    }

    public Nodo consultarNodo(String nombre){
        return getNodos().get(nombre);
    }
    

    public Map<String,Nodo> getNodos() {
        return elementos;
    }


    public void setNodos(Map<String,Nodo> elementos) {
        fechaUltimaModificacion=new Timestamp(new Date().getTime());
        this.elementos = elementos;
    }

}


