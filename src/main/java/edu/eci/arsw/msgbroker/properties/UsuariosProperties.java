/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.properties;

import edu.eci.arsw.msgbroker.services.PersistenciaUsuarios;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;

/**
 *
 * @author JOSEPH
 */
@Service
public class UsuariosProperties implements PersistenciaUsuarios{
    
    private Map<String, String> users;
    
    //Constructor de los usuarios
    public UsuariosProperties(){
        users=new HashMap<>();
        //Agregar usuario
        agregarUsuarios();
    }
    
    
    @Override
    public boolean consultarUsuario(String nombre, String contra) {
        return users.containsKey(nombre) && users.get(nombre).equals(contra);
    }

    
    //Poblar usuarios (Datos)
    
    private void agregarUsuarios(){
      
        users.put("Leslie Medina", "d2356f5e577460169e8d1b7b6e8d2858c2ab0b14");
        users.put("Johan Ramirez", "ee069232879eb762cc9885efa3892938b550f014");
        users.put("Joseph Diaz", "932e4f85ee23c34687c3aca2f7b3fca63ccdf207");
        users.put("Hector Cadavid", "e386425ec7fc123232fae4eb805c6b0749e8f0e8");
      
    }
}
