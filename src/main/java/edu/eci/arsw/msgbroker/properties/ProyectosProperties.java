/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.properties;

import edu.eci.arsw.msgbroker.model.Proyecto;
import edu.eci.arsw.msgbroker.services.PersistenciaProyecto;
import edu.eci.arsw.msgbroker.services.ProyectoUsuario;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author TatianaM
 */

@Service
public class ProyectosProperties implements PersistenciaProyecto{

    private Map<String, List<String>> usuarios;
    private Map<String, Proyecto> proyectos;

    public ProyectosProperties() throws Exception {
        usuarios = new ConcurrentHashMap<>();
        proyectos = new ConcurrentHashMap<>();
        cargarProyectos();
    }

    /**
     * @return the usuarios
     */
    public Map<String, List<String>> getUsuarios() {
        return usuarios;
    }

    /**
     * @param usuarios the usuarios to set
     */
    public void setUsuarios(Map<String, List<String>> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the proyectos
     */
    public Map<String, Proyecto> getProyectos() {
        return proyectos;
    }

    /**
     * @param proyectos the proyectos to set
     */
    public void setProyectos(Map<String, Proyecto> proyectos) {
        this.proyectos = proyectos;
    }


    @Override
    public Map<String, Proyecto> consultarProyectosUsuario(String usuario) throws Exception {
        validarUsuario(usuario);
        Map<String, Proyecto> proy = new HashMap<>();
        usuarios.get(usuario).stream().forEach((name) -> {
            proy.put(name, proyectos.get(name));
        });
        return proy;
    }

    @Override
    public Map<String, Map<String, Proyecto>> consultarProyectos() {
        Map<String, Map<String, Proyecto>> todos = new HashMap<>();
        usuarios.keySet().stream().forEach((nombre) -> {
            try {
                todos.put(nombre, consultarProyectosUsuario(nombre));
            } catch (Exception ex) {
                Logger.getLogger(ProyectoUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        return todos;
    }

    @Override
    public Proyecto consultarProyectoUsuario(String usuario, String proyecto) throws Exception {
        validarUsuario(usuario);
        if (!usuarios.get(usuario).contains(proyecto)) {
            throw new Exception("El usuario " + usuario + " no colabora en el proyecto " + proyecto);
        }
        return proyectos.get(proyecto);
    }

    @Override
    public void agregarProyecto(String usuario, Proyecto proyecto) throws Exception {
        validarUsuario(usuario);
        
        if (usuarios.get(usuario).contains(proyecto.getNombre())) {
            throw new Exception("El usuario " + usuario + " ya colabora en el proyecto " + proyecto.getNombre());
        }
        usuarios.get(usuario).add(proyecto.getNombre());
        proyectos.put(proyecto.getNombre(), proyecto);
    }

    @Override
    public void actualizarProyecto(String usuario, Proyecto proyecto) throws Exception {
        validarUsuario(usuario);
        if (!usuarios.get(usuario).contains(proyecto.getNombre())) {
            throw new Exception("El usuario " + usuario + " aun no colabora en el proyecto " + proyecto.getNombre());
        }
        proyectos.put(proyecto.getNombre(), proyecto);
    }

    @Override
    public void agregarUsuario(String usuario) throws Exception {
        if (usuarios.containsKey(usuario)) {
            throw new Exception("El usuario " + usuario + " ya se encuentra registrado");
        }
        usuarios.put(usuario, new ArrayList<>());
    }

    /**
     * Se encarga de validar que el nombre de usuario sea valido
     *
     * @param usuario el usuario a consultar
     * @throws Exception si el nombre de usuario no existe
     */
    private void validarUsuario(String usuario) throws Exception {
        if (!usuarios.containsKey(usuario)) {
            throw new Exception("El usuario " + usuario + " no se encuentra registrado");
        }
    }
    /**
     * Cargar los elementos de proyectos
     * @throws java.lang.Exception
     */
    private void cargarProyectos() throws Exception {
        
        List<String> nomProyectos1 = new ArrayList<>();
        List<String> nomProyectos2 = new ArrayList<>();
        List<String> nomProyectos3 = new ArrayList<>();
        //Proyectos
        Proyecto p1 = new Proyecto("Proyecto 1", "primer proyecto");
        Proyecto p2 = new Proyecto("Proyecto 2", "segundo proyecto");
        Proyecto p3 = new Proyecto("Proyecto 3", "tercer proyecto");

        //Agregando proyectos
        proyectos.put(p1.getNombre(), p1);
        proyectos.put(p2.getNombre(), p2);
        proyectos.put(p3.getNombre(), p3);

        //Agregando usuarios y sus proyectos
        nomProyectos1.add(p2.getNombre());
        nomProyectos2.add(p3.getNombre());
        nomProyectos3.add(p1.getNombre());

        usuarios.put("Joseph Diaz", nomProyectos1);
        usuarios.put("Johan Ramirez", nomProyectos2);
        usuarios.put("Leslie Medina", nomProyectos3);
        
        agregarUsuario("1");
        agregarUsuario("2");
        agregarUsuario("3");

    }
    
}
