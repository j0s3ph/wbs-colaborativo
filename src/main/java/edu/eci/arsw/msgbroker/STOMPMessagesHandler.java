/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker;

import edu.eci.arsw.msgbroker.model.Line;
import edu.eci.arsw.msgbroker.model.Nodo;
import edu.eci.arsw.msgbroker.model.Point;
import edu.eci.arsw.msgbroker.model.relation;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

/**
 *
 * @author 2096898
 */
@Controller
public class STOMPMessagesHandler {

    @Autowired
    SimpMessagingTemplate msgt;

    Point[] myList = new Point[4];
    Point[] myList2 = new Point[4];
    ArrayList<Nodo> Nodos= new ArrayList<>();
    ArrayList<Line> relaciones= new ArrayList<>();
    int i = 0;
    String nom=null;

    @MessageMapping("/newpoint.{num}")
    public void getLine(Point pt, @DestinationVariable String num) throws Exception {
        msgt.convertAndSend("/topic/newpoint." + num, pt);

        myList[0] = new Point(pt.getX(), pt.getY());
        myList[1] = new Point(pt.getX() , pt.getY()+40);
        myList[2] = new Point(pt.getX() + ((nom.length())*8), pt.getY() + 40);
        myList[3] = new Point(pt.getX() + ((nom.length())*8), pt.getY());     

        if (myList[3] != null) {
            msgt.convertAndSend("/topic/newpolygon." + num, myList);
            myList = new Point[4];
        }
    }
    
    @MessageMapping("/newname.{num}")
    public void getnewname(String pt, @DestinationVariable String num) throws Exception {
        
        msgt.convertAndSend("/topic/newname." + num, pt);
        nom=pt;
    }
    
    @MessageMapping("/newNodo.{num}")
    public void getNodo(Nodo pt, @DestinationVariable String num) throws Exception {

        Nodos.add(pt);
    }

    @MessageMapping("/buscar.{num}")
    public void getNodos(relation pt, @DestinationVariable String num) throws Exception {
        int x1=0,y1=0,x2=0,y2=0;
        Line a;
        for (int i=0;i<Nodos.size();i++ ){
           if (Nodos.get(i).getName().equals(pt.getName1())){
               x1=Nodos.get(i).getX();
               x1+=(((Nodos.get(i).getName().length())*4));
               y1=Nodos.get(i).getY();      
           }
           if (Nodos.get(i).getName().equals(pt.getName2())){
               x2=Nodos.get(i).getX();
               x2+=(((Nodos.get(i).getName().length())*4));
               y2=Nodos.get(i).getY(); 
           }
        }
        a=new Line (x1,y1,x2,y2);
        myList2[0] = new Point(x1, y1+40);
        myList2[1] = new Point(x2 , y2);
        myList2[2] = new Point(a.getX1() , a.getY1());
        myList2[3] = new Point(a.getX(), a.getY());     
        relaciones.add(a);
        if (myList2[3] != null) {
            msgt.convertAndSend("/topic/newpolygon2." + num, myList2);
            myList2 = new Point[4];
        }

    }
    
    @MessageMapping("/modificar.{num}")
    public void Modificar(relation pt, @DestinationVariable String num) throws Exception {
        int x1=0,y1=0,x2=0,y2=0;
        boolean a=false;
        for (int i=0;i<Nodos.size();i++ ){
           if (Nodos.get(i).getName().equals(pt.getName1())){
               a=true;
               x1=Nodos.get(i).getX();
               y1=Nodos.get(i).getY();
               Nodos.get(i).setName(pt.getName2());
           }

        }
        if (a){
            myList[0] = new Point(x1, y1);
            myList[1] = new Point(x1 , y1+40);
            myList[2] = new Point(x1 + ((nom.length())*8), y1 + 40);
            myList[3] = new Point(x1 + ((nom.length())*8), y1);     
            msgt.convertAndSend("/topic/newpolygon3." + num, myList);
            msgt.convertAndSend("/topic/newpolygon." + num, myList);
            myList = new Point[4];
        }
    }
    
}
