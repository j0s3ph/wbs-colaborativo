/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.services;

import edu.eci.arsw.msgbroker.model.Proyecto;
import java.util.Map;

/**
 *
 * @author TatianaM
 */
public interface PersistenciaProyecto {

    public Map<String,Proyecto> consultarProyectosUsuario(String usuario) throws Exception;
    

    public Map<String,Map<String,Proyecto>> consultarProyectos();
    

    public Proyecto consultarProyectoUsuario(String usuario,String proyecto) throws Exception;
    

    public void agregarProyecto(String usuario,Proyecto proyecto) throws Exception;

    
    public void actualizarProyecto(String usuario,Proyecto proyecto) throws Exception;
    

    public void agregarUsuario(String usuario) throws Exception;
    
}
