/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.services;

import edu.eci.arsw.msgbroker.model.Proyecto;
import edu.eci.arsw.msgbroker.properties.ProyectosProperties;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author TatianaM
 */

@Service
public class ProyectoUsuario {

    @Autowired 
    private PersistenciaProyecto persistencia;

    public PersistenciaProyecto getPersistencia() {
        return persistencia;
    }

    public void setPersistencia(PersistenciaProyecto persistencia) {
        this.persistencia = persistencia;
    }
    

    public ProyectoUsuario() throws Exception{
        persistencia= new ProyectosProperties();
        System.out.println("ESTOY EN CARGAR PROYPU");
    }
    public ProyectoUsuario(PersistenciaProyecto persistencia){
        this.persistencia=persistencia;
    }
    

    public Map<String,Proyecto> consultarProyectosUsuario(String usuario) throws Exception{
        return getPersistencia().consultarProyectosUsuario(usuario);
    }
    

    public Map<String,Map<String,Proyecto>> consultarProyectos(){
        return getPersistencia().consultarProyectos();
    }
    

    public Proyecto consultarProyectoUsuario(String usuario,String proyecto) throws Exception{
        return getPersistencia().consultarProyectoUsuario(usuario, proyecto);
    }
    

    public void agregarProyecto(String usuario,Proyecto proyecto) throws Exception{
        getPersistencia().agregarProyecto(usuario, proyecto);
    }
    

    public void actualizarProyecto(String usuario,Proyecto proyecto) throws Exception{
        getPersistencia().actualizarProyecto(usuario, proyecto);
    }
    

    public void agregarUsuario(String usuario) throws Exception{
        getPersistencia().agregarUsuario(usuario);
    }
    
}
