/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 2096129
 */
@Service
public class SeguridadUsuario {
    @Autowired
    private PersistenciaUsuarios peru;
    
    public PersistenciaUsuarios getperu() {
        return peru;
    }
     public void setperu(PersistenciaUsuarios peru) {
        this.peru = peru;
    }
     
    public boolean consultarUsuario(String nombre, String contra) {
        return peru.consultarUsuario(nombre, contra);
    } 
}