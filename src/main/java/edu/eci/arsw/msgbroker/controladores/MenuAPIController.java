/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.controladores;

import edu.eci.arsw.msgbroker.model.Proyecto;
import edu.eci.arsw.msgbroker.services.ProyectoUsuario;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/menu")
public class MenuAPIController {
    
    @Autowired
    ProyectoUsuario pUsuarios;


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> consultarProyectos() {
        Map<String, Map<String, Proyecto>> proyectos = pUsuarios.consultarProyectos();
        return new ResponseEntity<>(proyectos, HttpStatus.ACCEPTED);
    }


    @RequestMapping(path = "/users/{userid}", method = RequestMethod.GET)
    public ResponseEntity<?> consultarProyectosUsuario(@PathVariable String userid) {
        try {
            Map<String, Proyecto> proyectosUsuario = pUsuarios.consultarProyectosUsuario(userid);
            return new ResponseEntity<>(proyectosUsuario, HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }

    }


    @RequestMapping(path = "/users/{userid}/{projectid}", method = RequestMethod.GET)
    public ResponseEntity<?> consultarProyectoUsuario(@PathVariable String projectid, @PathVariable String userid) {
        try {
            Proyecto proyectoUsuario = pUsuarios.consultarProyectoUsuario(userid, projectid);
            return new ResponseEntity<>(proyectoUsuario, HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(path = "/users/{userid}", method = RequestMethod.POST)
    public ResponseEntity<?> agregarProyecto(@RequestBody Proyecto project, @PathVariable String userid) {
        try {
            pUsuarios.agregarProyecto(userid, project);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(path = "/users/{userid}", method = RequestMethod.PUT)
    public ResponseEntity<?> actualizarProyecto(@PathVariable String userid, @RequestBody Proyecto project) {
        try {
            pUsuarios.actualizarProyecto(userid, project);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.NOT_MODIFIED);
        }
    }


    @RequestMapping(path = "/users", method = RequestMethod.PUT)
    public ResponseEntity<?> agregarUsuario(@PathVariable String userid) {
        try {
            pUsuarios.agregarUsuario(userid);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.NOT_MODIFIED);
        }
    }
}
