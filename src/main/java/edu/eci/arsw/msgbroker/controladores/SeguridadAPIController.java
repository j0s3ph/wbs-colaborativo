/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.controladores;

import edu.eci.arsw.msgbroker.services.SeguridadUsuario;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 2096898
 */

@Service
@RestController
@RequestMapping(value = "/userLogin")
public class SeguridadAPIController {
    HashMap<Integer, List<String>> dictionary = new HashMap<>();
    List<String> data;
    
    
    @Autowired
    SeguridadUsuario segi=new SeguridadUsuario();
    

    @RequestMapping(path = "/{nombre}/{contra}", method = RequestMethod.GET)
    public ResponseEntity<?> validarUsuario(@PathVariable String nombre,@PathVariable String contra){
      return new ResponseEntity<>(segi.consultarUsuario(nombre, contra),HttpStatus.ACCEPTED);
    }

}
