/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.controladores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 2096898
 */

@Service
@RestController
@RequestMapping(value = "/dibujos")
public class DibujoAPIController {
    HashMap<Integer, List<String>> dictionary = new HashMap<>();
    List<String> data;
    
    @RequestMapping(path = "/{iddibujo}", method = RequestMethod.PUT)
    public ResponseEntity<?> manejadorGetMesa(@PathVariable int iddibujo, @RequestBody String nombre) {
        List<String> col = new ArrayList();
        try {
            if (dictionary.get(iddibujo) == null){
                col.add(nombre);
                dictionary.put(iddibujo, col);
            }else{
                col = dictionary.get(iddibujo);
                col.add(nombre);
                dictionary.put(iddibujo, col);
            }
            dictionary.put(iddibujo, col);
            return new ResponseEntity<>(dictionary, HttpStatus.ACCEPTED);

        } catch (Exception ex) {
            Logger.getLogger(DibujoAPIController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>("Error 404", HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(path = "/{iddibujo}/colaboradores", method = RequestMethod.GET)
    public ResponseEntity<?> manejadorGetTotalMesa(@PathVariable int iddibujo) {
        try {
            data = dictionary.get(iddibujo);
            return new ResponseEntity<>(data, HttpStatus.ACCEPTED);

        } catch (Exception ex) {
            Logger.getLogger(DibujoAPIController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>("Error 404", HttpStatus.NOT_FOUND);
        }
    }
}
