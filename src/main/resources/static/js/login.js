
usuario="";
function loginval(){
    usuario=$("#username").val();
    var contra=hex_sha1($("#passwd").val());
    verificar(contra);
}
function redireccion(){
    sessionStorage.name=usuario;
    window.location.href='menuproy.html';
};

function oculto(){
    $("#mensaje").hide();
}
function verificar(contra){
    return $.get("/userLogin/"+usuario+"/"+contra, function(data){
            if(data){
                redireccion();
            }else{
                $("#mensaje").show();
            }
    }).fail(function (){
                $("#mensaje").show();
    } );
}
$(document).ready(
        function () {
            oculto();
            sessionStorage.name="";
        }
);