/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var radioButton1 = '<input type="radio" name="proyecto" value="';
var radioButton2 = '" checked> Seleccionar<br>';
var proyect = null;
var numproyect = 0;
var proyectList = [];

function connect() {
    if (sessionStorage.connected != true) {
        sessionStorage.connected = true;
        var socket = new SockJS('/stompendpoint');

        stompClient = Stomp.over(socket);
        console.info(stompClient);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/topic/newproject.' + sessionStorage.name, function (data) {
                console.log("llego");
                var objeto = JSON.parse(data.body);
                objeto.fechaCreacion = new Date(objeto.fechaCreacion);
                objeto.fechaUltimaModificacion = new Date(objeto.fechaUltimaModificacion);
                addMenu(objeto);
            });
        });
    }
};

function disconnect() {
    sessionStorage.connected = false;
    if (stompClient != null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
};

function addPopUp() {
    $("#newP").show();
};

function acceder() {
    disconnect();
    sessionStorage.nameProject = $('input[name=proyecto]:checked').val();
    window.location.href = 'proyecto.html';
};

function init() {
    $("#usrnm").html("Usuario: " + sessionStorage.name);
    //disconnect();
    connect();
    getProy();
    sessionStorage.nameProject = "";
};

function addProy() {
    console.log($("#nomP").val());
    console.log($("#descP").val());
    $("#newP").hide();
    proyect = {"nombre": $("#nomP").val(), "descripcion": $("#descP").val(), "fechaCreacion": new Date(), "fechaUltimaModificacion": new Date(), "diagramas" : {}};
    postProy(proyect);

};

function addMenu(proy) {
    numproyect++;
    proyectList[proy.nombre] = proy;
    $("#lista").append("<tr><td>" + proy.nombre + "</td><td>" + proy.descripcion + "</td><td>" + proy.fechaCreacion.toLocaleString() +
            "</td><td>" + proy.fechaUltimaModificacion.toLocaleString() + "</td><td>" + radioButton1 + proy.nombre + radioButton2 + "</td>" + "</tr>");
};

sendProject = function () {
    stompClient.send('/app/newproject.' + sessionStorage.name, {}, JSON.stringify(proyect));
};

function getProy() {
    return $.get("/menu/users/" + sessionStorage.name, function (data) {
        for (var element in data) {
            data[element].fechaCreacion = new Date(data[element].fechaCreacion);
            data[element].fechaUltimaModificacion = new Date(data[element].fechaUltimaModificacion);
            addMenu(data[element]);
        };
    }).fail(function (response) {
        $("#mesje").html(response.responseText);
        $("#errMes").show();
    });
};

function postProy(proyect) {
    console.log(JSON.stringify(proyect));
    console.log("/menu/users/" + sessionStorage.name);
    return $.ajax({
        url: "/menu/users/" + sessionStorage.name,
        type: 'POST',
        data: JSON.stringify(proyect),
        contentType: "application/json"
    }).fail(function (response) {
        $("#mesje").html(response.responseText);
        $("#errMes").show();
    }).then(newProyPost);
};

function newProyPost(){
    $("#nU").hide();
    $("#mesjs").html("Se ha creado el proyecto " + proyect.nombre);
    $("#succMes").show();
    sendProject();
}

function check() {
    if (sessionStorage.name === null || sessionStorage.name.length === 0) {
        back();
    }
};

function back(){
    sessionStorage.nameProject="";
    window.location.href = 'index.html';
}

$(document).ready(
        function () {
            check();
            init();
        }
);
