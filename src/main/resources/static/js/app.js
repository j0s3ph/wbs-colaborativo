var stompClient = null;
var grupos = null;
var usuario = null;
var nombre = null;
var estado = "relacion";

function relacion() {
    var nombre2 = prompt("Por favor, ingrese el nombre del nodo 1: ", "");
    var nombre3 = prompt("Por favor, ingrese el nombre del nodo 2: ", "");
    
    stompClient.send("/app/buscar." + grupos, {}, JSON.stringify({name1:nombre2, name2:nombre3 }));
}


function Modificar() {
    
    var nombre2 = prompt("Por favor, ingrese el nombre del nodo que quiere modificar: ", "");
    var nombre3 = prompt("Por favor, ingrese el nuevo nombre del nodo: ", "");
    if (nombre2!=null && nombre3!=null){
        stompClient.send("/app/newname." + grupos, {}, JSON.stringify({nom:nombre3}));
        stompClient.send("/app/modificar." + grupos, {}, JSON.stringify({name1:nombre2, name2:nombre3 }));
    }
    
}

function back(){
    sessionStorage.nameProject="";
    window.location.href = 'menuproy.html';
}

function pro(){
    var canvas = document.getElementById('myCanvas');
    var width = ((canvas.width)/2)-20; 
    stompClient.send("/app/newname." + grupos, {}, JSON.stringify({nom:"proyecto"}));
    stompClient.send("/app/newpoint." + grupos, {}, JSON.stringify({x:width , y:15 }));
    stompClient.send("/app/newNodo." + grupos, {}, JSON.stringify({x: width, y: 15, name:"proyecto" }));}
   
function gerpro(){
    stompClient.send("/app/newname." + grupos, {}, JSON.stringify({nom:"Gerencia de proyectos"}));
    stompClient.send("/app/newpoint." + grupos, {}, JSON.stringify({x:1000, y:93 }));
    stompClient.send("/app/newNodo." + grupos, {}, JSON.stringify({x: 1000, y: 93, name:"Gerencia de proyectos" }));
    
}
function pru(){
    stompClient.send("/app/buscar." + grupos, {}, JSON.stringify({name1:"proyecto", name2:"Gerencia de proyectos" }));
}
function plantilla(){
    pro();
    gerpro();
    pru();
}

function connect() {
    var socket = new SockJS('/stompendpoint');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);


        stompClient.subscribe('/topic/newpolygon.' + grupos, function (data) {
            var coordenadas = JSON.parse(data.body);
            var canvas = document.getElementById('myCanvas');
            var context = canvas.getContext('2d');
            context.fillStyle = '#E4EDF0';
            context.beginPath();
            context.moveTo(coordenadas[0].x, coordenadas[0].y);
            for (var i = 1; i < coordenadas.length; i++) {
                context.lineTo(coordenadas[i].x, coordenadas[i].y);
            }
            context.closePath();
            context.fill();
            context.font = "20px Arial";
            context.strokeText(nombre, coordenadas[0].x+15, coordenadas[0].y+20 );
        });

        stompClient.subscribe('/topic/newpolygon2.' + grupos, function (data) {
            var coordenadas = JSON.parse(data.body);
            console.log(coordenadas);
            var canvas = document.getElementById('myCanvas');
            var context = canvas.getContext('2d');
            context.fillStyle = '#E4EDF0';
            context.beginPath();
            context.moveTo(coordenadas[0].x, coordenadas[0].y);
            context.lineTo(coordenadas[1].x, coordenadas[1].y);
            context.stroke();
            context.closePath();
            context.fill();
        });
        
        stompClient.subscribe('/topic/newpolygon3.' + grupos, function (data) {
            var coordenadas = JSON.parse(data.body);
            var canvas = document.getElementById('myCanvas');
            var context = canvas.getContext('2d');
            context.fillStyle = '#E4EDF0';
            context.beginPath();
            context.clearRect(coordenadas[0].x,coordenadas[0].y, (coordenadas[2].x-coordenadas[0].x), 40);
            context.closePath();
            context.fill();
        });

        stompClient.subscribe('/topic/newname.' + grupos, function (data) {
            var coordenadas = JSON.parse(data.body);
            nombre=coordenadas.nom;
        });

        stompClient.subscribe('/topic/newrelation.' + grupos, function (data) {
            var coordenadas = JSON.parse(data.body);
            var canvas = document.getElementById('myCanvas');
            var context = canvas.getContext('2d');
            context.lineWidth = 3;
            context.strokeStyle = "#f01";
            context.beginPath();
            context.moveTo(coordenadas.x, coordenadas.y);
            context.lineTo(coordenadas.x1, coordenadas.y1);
            context.stroke();
            context.closePath();
        });

    });
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function grupoColaborador() {
    if (sessionStorage.name !== null && sessionStorage.nameProject !== null) {
        grupos = sessionStorage.nameProject;
        usuario = sessionStorage.name;
    }
    $("#usuario").html("Usuario: " + usuario);
    $("#proyecto").html("Proyecto: " + grupos);
    $(".brand").html("Collaborative WBS - " + grupos);
}

function doPut() {
    $.ajax("/dibujos/" + grupos, {
        type: "PUT",
        data: JSON.stringify(usuario),
        contentType: "application/json",
        success: function () {
            console.log(usuario);
        }
    });
};


$(document).ready(
        function () {
            connect();
            grupoColaborador();
            console.info('connecting to websockets');
            doPut();
            function getMousePos(canvas, evt) {
                var rect = canvas.getBoundingClientRect();
                return {
                    pX: evt.clientX - rect.left,
                    pY: evt.clientY - rect.top
                };
            }
            var canvas = document.getElementById('myCanvas');
            var context = canvas.getContext('2d');
            canvas.addEventListener('mousedown', function (evt) {
                var mousePos = getMousePos(canvas, evt);
                var ctx = canvas.getContext("2d");
                   
                nombre = prompt("Por favor, ingrese el nombre del nodo que desea ingresar: ", "");
                if (nombre!==null){
                    stompClient.send("/app/newname." + grupos, {}, JSON.stringify({nom:nombre}));
                    stompClient.send("/app/newpoint." + grupos, {}, JSON.stringify({x: mousePos.pX, y: mousePos.pY}));
                    stompClient.send("/app/newNodo." + grupos, {}, JSON.stringify({x: mousePos.pX, y: mousePos.pY, name:nombre }));
                    
                    //stompClient.send("/app/newrelation." + grupos, {}, JSON.stringify({x: 0, y: 0,x1: 100, y1: 100}));
                }
            }, false);
        }
);
